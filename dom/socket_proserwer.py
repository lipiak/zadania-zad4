# -*- coding: utf-8 -*-


# -*- coding: utf-8 -*-

import socket
import sys
import os
import logging
from SocketServer import ThreadingMixIn, TCPServer, BaseRequestHandler

from daemon import runner

from mimetypes import MimeTypes

# Ustawienie loggera
logger = logging.getLogger("ProserwerDaemonLog")
logger.setLevel(logging.INFO)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
handler = logging.FileHandler("demon.log")
handler.setFormatter(formatter)
logger.addHandler(handler)


class ProserwerHandler(BaseRequestHandler):

    def handle(self):
        http_serve(self.request, logger)


class ProserwerServer(ThreadingMixIn, TCPServer):
    allow_reuse_address = 1


class Demon:
    def __init__(self):
        self.stdin_path = '/dev/null'
        self.stdout_path = '/dev/tty'
        self.stderr_path = '/dev/tty'
        self.pidfile_path = '/tmp/proserwerk8r97tgy4uyhijkr3thgt.pid'
        self.pidfile_timeout = 5

    #Kod serwera
    def run(self):
        serwer = ProserwerServer(('194.29.175.240', 23678), ProserwerHandler)
        serwer.serve_forever()
        #server(logger)


def handle(request, logger):

    mime = MimeTypes()
    #sprawdzic czy zapytanie jest zapytaniem GET HTTP
    podzielone = request.split(' ')

    if podzielone[0] == 'GET' and (podzielone[2])[:4] == 'HTTP':

        #sprawdzic, czy plik lub katalog, ktorego zadamy istnieje
        zadany_plik = 'home/p9/web' + podzielone[1]
        logger.info(u'Zadany plik: {0}'.format(os.path.abspath(zadany_plik)))
        if os.path.exists(zadany_plik):

            #teraz dwie opcje, jesli katalog, wylistowac pliki
            #jest plik, otworzyc i zwrocic zawartosc z content typem

            #jesli to jest tylko katalog
            try:
                if os.path.isdir(zadany_plik[:-1]) or os.path.isdir(zadany_plik):
                    logger.info(u'Żądanie dostępu do katalogu: {0}'.format(zadany_plik))
                    wszystko = os.listdir(zadany_plik)

                    content = '<ul>'
                    #generowanie odpowiedzi
                    for plik in wszystko:

                        #czy byl / na koncu zapytania
                        if zadany_plik[-1] == '/':
                            content += '<li><a href="' + podzielone[1] + plik + '">' + plik + '</a></li>'
                        else:
                            content += '<li><a href="' + podzielone[1] + '/' + plik + '">' + plik + '</a></li>'


                    content += '</ul>'

                    #kompletowanie odpowiedzi od serwera
                    header = 'HTTP/1.1 200 OK \r\n'
                    metadata = 'Content-Type: text/html \r\n'
                    logger.info(u'Odpowiedź: {0}'.format(header + metadata + '\r\n' + content + '\r\n'))
                    return header + metadata + '\r\n' + content + '\r\n'

            finally:
                #return 'HTTP/1.1 505 Internal Server Error\r\n'

                #a moze to jest plik a nie katalog?
                if os.path.isfile(zadany_plik):
                    logger.info(u'Żądanie dostępu do pliku: {0}'.format(zadany_plik))
                    header = 'HTTP/1.1 200 OK\r\n'
                    metadata = 'Content-Type: ' + mime.guess_type(zadany_plik)[0] + '\r\n'

                    if zadany_plik.endswith('.png'):
                        metadata = 'Content-Type: image/png\r\n'

                    content = open(zadany_plik, 'rb').read()
                    return header + metadata + '\r\n' + content + '\r\n'

        else:
            logger.info(u'Żądanie dostępu do nieistniejącej ścieżki: {0}'.format(zadany_plik))
            return 'HTTP/1.1 404 NOT FOUND\r\nContent-Type: text/html\r\n\r\n<html><body>404 NOT FOUND</body></html>\r\n'

    else:
        logger.info(u'Nieobsługiwany format zapytania HTTP')
        return 'HTTP/1.1 403 BAD REQUEST\r\nContent-Type: text/html\r\n\r\n<html><body>403 BAD REQUEST</body></html>\r\n'


def http_serve(gniazdo, logger):

    try:
        while True:
            # Czekanie na połączenie
            #connection, client_address = server_socket.accept()
            #logger.info(u'połączono z {0}:{1}'.format(*client_address))

            #odbieranie żądania
            request = ''
            done = False
            buffer = 4096

            logger.info(u'Odbieram zapytanie od: {0}'.format(socket))
            while not done:
                part = gniazdo.recv(buffer)
                if not part:
                    raise EOFError('NIE PYKŁO HEHE')

                if len(part) < buffer:
                    done = True
                request += part


            logger.info(u'Zapytanie od {0}: {1}'.format(socket, request))
            odp = handle(request, logger)
            gniazdo.sendall(odp)

    except EOFError as e:
        gniazdo.close()



#logging.config.fileConfig('logging.conf')
#logger = logging.getLogger('http_server')
#server(logger)
#sys.exit(0)
demon = Demon()
demon_runner = runner.DaemonRunner(demon)
demon_runner.daemon_context.files_preserve = [handler.stream]
demon_runner.do_action()
sys.exit(0)







