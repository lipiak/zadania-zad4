# -*- coding: utf-8 -*-

from funkload.FunkLoadTestCase import FunkLoadTestCase
import socket
import os
import unittest
import logging


SERVER_HOST = os.environ.get('SERVER', '194.29.175.240')
SERVER_PORT = int(os.environ.get('PORT', 23678))
spodziewana_odp = 'HTTP/1.1 200 OK \r\nContent-Type: text/html \r\n\r\n<ul><li><a href="/katalog">katalog</a></li><li><a href="/output">output</a></li></ul>\r\n'



class TestSerwer(FunkLoadTestCase):
    def test_dialog(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((SERVER_HOST, SERVER_PORT))
        sock.sendall('GET / HTTP/1.1')

        answer = ''
        done = False
        buffer = 4096

        while not done:
            part = sock.recv(buffer)

            if len(part) < buffer:
                done = True
            answer += part


        self.assertEqual(answer, spodziewana_odp)
        sock.close()


if __name__ == '__main__':
    unittest.main()